package com.example.demo.model;

import java.util.Arrays;

public class DocAndFile {
    private Document document;
    private File[] file;

    public DocAndFile() {}
    public DocAndFile(Document document, File[] file) {
        this.document = document;
        this.file = file;
    }

    public Document getDocument() {
        return document;
    }

    public void setDocument(Document document) {
        this.document = document;
    }

    public File[] getFile() {
        return file;
    }

    public void setFile(File[] file) {
        this.file = file;
    }

    @Override
    public String toString() {
        return "DocAndFile{" +
                "document=" + document +
                ", file=" + Arrays.toString(file) +
                '}';
    }
}
