package com.example.demo.model;

import org.springframework.lang.Nullable;

public class Document {
    private Long    id;
    private String  doc_id;
    private String  doc_category_id;
    private String  doc_category_name;
    private Boolean doc_category_new;
    private Long    user_id;
    private String  doc_type_name;
    private String  username;
    private String  doc_name;
    private String  doc_content;
    private String  doc_created_date;
    private String  doc_status;
    private Long    doc_view_amount;


    public Document() {}

    public Document(Long id, String doc_id, String doc_category_id, String doc_category_name, Boolean doc_category_new, Long user_id, String doc_type_name, String username, String doc_name, String doc_content, String doc_created_date, String doc_status, Long doc_view_amount) {
        this.id = id;
        this.doc_id = doc_id;
        this.doc_category_id = doc_category_id;
        this.doc_category_name = doc_category_name;
        this.doc_category_new = doc_category_new;
        this.user_id = user_id;
        this.doc_type_name = doc_type_name;
        this.username = username;
        this.doc_name = doc_name;
        this.doc_content = doc_content;
        this.doc_created_date = doc_created_date;
        this.doc_status = doc_status;
        this.doc_view_amount = doc_view_amount;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDoc_id() {
        return doc_id;
    }

    public void setDoc_id(String doc_id) {
        this.doc_id = doc_id;
    }

    public String getDoc_category_id() {
        return doc_category_id;
    }

    public void setDoc_category_id(String doc_category_id) {
        this.doc_category_id = doc_category_id;
    }

    public String getDoc_category_name() {
        return doc_category_name;
    }

    public void setDoc_category_name(String doc_category_name) {
        this.doc_category_name = doc_category_name;
    }

    public Boolean getDoc_category_new() {
        return doc_category_new;
    }

    public void setDoc_category_new(Boolean doc_category_new) {
        this.doc_category_new = doc_category_new;
    }

    public Long getUser_id() {
        return user_id;
    }

    public void setUser_id(Long user_id) {
        this.user_id = user_id;
    }

    public String getDoc_type_name() {
        return doc_type_name;
    }

    public void setDoc_type_name(String doc_type_name) {
        this.doc_type_name = doc_type_name;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getDoc_name() {
        return doc_name;
    }

    public void setDoc_name(String doc_name) {
        this.doc_name = doc_name;
    }

    public String getDoc_content() {
        return doc_content;
    }

    public void setDoc_content(String doc_content) {
        this.doc_content = doc_content;
    }

    public String getDoc_created_date() {
        return doc_created_date;
    }

    public void setDoc_created_date(String doc_created_date) {
        this.doc_created_date = doc_created_date;
    }

    public String getDoc_status() {
        return doc_status;
    }

    public void setDoc_status(String doc_status) {
        this.doc_status = doc_status;
    }

    public Long getDoc_view_amount() {
        return doc_view_amount;
    }

    public void setDoc_view_amount(Long doc_view_amount) {
        this.doc_view_amount = doc_view_amount;
    }

    @Override
    public String toString() {
        return "Document{" +
                "id=" + id +
                ", doc_id=" + doc_id +
                ", doc_category_id=" + doc_category_id +
                ", doc_category_name=" + doc_category_name +
                ", doc_category_new=" + doc_category_new +
                ", user_id=" + user_id +
                ", doc_type_name='" + doc_type_name + '\'' +
                ", username='" + username + '\'' +
                ", doc_name='" + doc_name + '\'' +
                ", doc_content='" + doc_content + '\'' +
                ", doc_created_date='" + doc_created_date + '\'' +
                ", doc_status='" + doc_status + '\'' +
                ", doc_view_amount=" + doc_view_amount +
                '}';
    }
}
