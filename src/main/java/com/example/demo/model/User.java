package com.example.demo.model;

public class User {
    private Long id;
    private Long user_role_id;
    private String username;
    private String password;
    private String user_created_date;
    private String user_status;

    public User() {}
    public User(Long id, Long user_role_id, String username, String password, String user_created_date, String user_status) {
        this.id = id;
        this.user_role_id = user_role_id;
        this.username = username;
        this.password = password;
        this.user_created_date = user_created_date;
        this.user_status = user_status;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getUser_role_id() {
        return user_role_id;
    }

    public void setUser_role_id(Long user_role_id) {
        this.user_role_id = user_role_id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUser_created_date() {
        return user_created_date;
    }

    public void setUser_created_date(String user_created_date) {
        this.user_created_date = user_created_date;
    }

    public String getUser_status() {
        return user_status;
    }

    public void setUser_status(String user_status) {
        this.user_status = user_status;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", user_role_id=" + user_role_id +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", user_created_date='" + user_created_date + '\'' +
                ", user_status='" + user_status + '\'' +
                '}';
    }
}
