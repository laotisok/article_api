package com.example.demo.model;

public class File {
    private Long id;
    private String doc_id;
    private String doc_file_name;
    private String doc_file_path;
    private String doc_file_status;
    private String doc_file_created_date;

    public File() { }
    public File(Long id, String doc_id, String doc_file_name, String doc_file_path, String doc_file_status, String doc_file_created_date) {
        this.id = id;
        this.doc_id = doc_id;
        this.doc_file_name = doc_file_name;
        this.doc_file_path = doc_file_path;
        this.doc_file_status = doc_file_status;
        this.doc_file_created_date = doc_file_created_date;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDoc_id() {
        return doc_id;
    }

    public void setDoc_id(String doc_id) {
        this.doc_id = doc_id;
    }

    public String getDoc_file_name() {
        return doc_file_name;
    }

    public void setDoc_file_name(String doc_file_name) {
        this.doc_file_name = doc_file_name;
    }

    public String getDoc_file_status() {
        return doc_file_status;
    }

    public void setDoc_file_status(String doc_file_status) {
        this.doc_file_status = doc_file_status;
    }

    public String getDoc_file_created_date() {
        return doc_file_created_date;
    }

    public void setDoc_file_created_date(String doc_file_created_date) {
        this.doc_file_created_date = doc_file_created_date;
    }

    public String getDoc_file_path() {
        return doc_file_path;
    }

    public void setDoc_file_path(String doc_file_path) {
        this.doc_file_path = doc_file_path;
    }

    @Override
    public String toString() {
        return "File{" +
                "id=" + id +
                ", doc_id='" + doc_id + '\'' +
                ", doc_file_name='" + doc_file_name + '\'' +
                ", doc_file_path='" + doc_file_path + '\'' +
                ", doc_file_status='" + doc_file_status + '\'' +
                ", doc_file_created_date='" + doc_file_created_date + '\'' +
                '}';
    }
}
