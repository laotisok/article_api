package com.example.demo.model;

public class Category {
    private Long id;
    private Long doc_user_id;
    private String doc_category_id;
    private String doc_category_name;
    private String doc_category_status;

    public Category() {}
    public Category(Long id, Long doc_user_id, String doc_category_id, String doc_category_name, String doc_category_status) {
        this.id = id;
        this.doc_user_id = doc_user_id;
        this.doc_category_id = doc_category_id;
        this.doc_category_name = doc_category_name;
        this.doc_category_status = doc_category_status;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getDoc_user_id() {
        return doc_user_id;
    }

    public void setDoc_user_id(Long doc_user_id) {
        this.doc_user_id = doc_user_id;
    }

    public String getDoc_category_id() {
        return doc_category_id;
    }

    public void setDoc_category_id(String doc_category_id) {
        this.doc_category_id = doc_category_id;
    }

    public String getDoc_category_name() {
        return doc_category_name;
    }

    public void setDoc_category_name(String doc_category_name) {
        this.doc_category_name = doc_category_name;
    }

    public String getDoc_category_status() {
        return doc_category_status;
    }

    public void setDoc_category_status(String doc_category_status) {
        this.doc_category_status = doc_category_status;
    }

    @Override
    public String toString() {
        return "Category{" +
                "id=" + id +
                ", doc_user_id=" + doc_user_id +
                ", doc_category_id='" + doc_category_id + '\'' +
                ", doc_category_name='" + doc_category_name + '\'' +
                ", doc_category_status='" + doc_category_status + '\'' +
                '}';
    }
}
