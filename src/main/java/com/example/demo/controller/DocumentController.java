package com.example.demo.controller;

import com.example.demo.exceptions.CleanFolder;
import com.example.demo.model.*;
import com.example.demo.service.DocumentService;
import com.example.demo.service.FileService;
import com.example.demo.service.UserService;
import com.google.cloud.storage.BlobId;
import com.google.cloud.storage.BlobInfo;
import com.google.cloud.storage.Storage;
import com.google.cloud.storage.StorageOptions;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;

@RestController
@RequestMapping(value = "/api/documents")
public class DocumentController {

    private DocumentService documentService;
    private FileService fileService;
    private UserService userService;

    @Autowired
    public void setDocumentService(DocumentService documentService){ this.documentService = documentService; }

    @Autowired
    public void setFileService(FileService fileService){ this.fileService = fileService; }

    @Autowired
    public void setUserService(UserService userService){ this.userService = userService; }

    @GetMapping("")
    public Map<String, Object> findAll(){
        List<Document> docs = documentService.findAll();
        List<File> files = fileService.findAll();
        Map<String, Object> map = new HashMap<>();
        map.put("data",docs);
        map.put("file",files);
        return map;
    }

    @GetMapping("/profile/{userId}")
    public Map<String, Object> findAllMine(@PathVariable("userId") Long userid){ //get all data of user
        List<Document> docs = documentService.findAllMine(userid);
        List<Category> categories = documentService.findAllMyCategory(userid);
        Map<String, Object> map = new HashMap<>();
        map.put("data",docs);
        map.put("categories",categories);
        return map;
    }

    @GetMapping("/login/{username}/{password}")
    public List<User> login(@PathVariable("username") String username, @PathVariable("password") String password){
        List<User> user = userService.login(username, password);
        System.out.println("TESTING: "+user);
        return user;
    }

    @GetMapping("/{id}")
    public Map<String, Object> findOne(@PathVariable("id") String id){  //get one data of user
        System.out.println("ID: "+id);
        try {
            deleteFile();
        } catch (IOException e) {
            System.out.println("Error to delete file: "); e.printStackTrace();
        }
        List<Document> doc = documentService.findOne(id);
        List<File> file = fileService.findFileOne(id);
        Map<String, Object> map = new HashMap<>();
        map.put("data",doc);
        map.put("file",file);
        return map;
    }

    @GetMapping("/docId/{docId}/userId/{userId}")
    public Map<String, Object> findOneUpdate(@PathVariable("docId") String docId, @PathVariable("userId") String userId){  //get one data of user for update have all category
        System.out.println("docId: "+docId);
        try {
            deleteFile();
        } catch (IOException e) {
            System.out.println("Error to delete file: "); e.printStackTrace();
        }
        List<Document> doc = documentService.findOne(docId);
        List<File> file = fileService.findFileOne(docId);
        List<Category> categories = documentService.findAllMyCategory(Long. parseLong(userId));
        Map<String, Object> map = new HashMap<>();
        map.put("data",doc);
        map.put("categories",categories);
        map.put("file",file);
        return map;
    }

    @PostMapping()
    public Map<String, Object> addDocument(@RequestBody DocAndFile docAndFile){
        if(docAndFile.getDocument().getDoc_category_new() == true){
            documentService.addCategory(docAndFile.getDocument());
        }

        List<Document> doc = documentService.addDocument(docAndFile.getDocument());

        System.out.println("doc: "+doc);
        JSONArray files = new JSONArray();
        for(int i=0; i< docAndFile.getFile().length; i++){
            List<File>    file = fileService.addFile(docAndFile.getFile()[i]);
            files.put(file);

            try {
                uploadObject(docAndFile.getFile()[i].getDoc_file_name(), "uploads/"+docAndFile.getFile()[i].getDoc_file_name());
            } catch (IOException e) {
                System.out.println("Error upload to google cloud: ");e.printStackTrace();
            }
        }
        Map<String, Object> map = new HashMap<>();
        map.put("doc",doc);
        map.put("files",files);
        System.out.println("files: "+files);

        try {
            deleteFile();
        } catch (IOException e) {
            System.out.println("Error to delete file: "); e.printStackTrace();
        }
        return map;
    }

    @PostMapping("/update")
    public Map<String, Object> updateDocument(@RequestBody DocAndFile docAndFile){
        if(docAndFile.getDocument().getDoc_category_new() == true){
            documentService.addCategory(docAndFile.getDocument());
        }
        System.out.println("DOC: "+docAndFile.getDocument().toString());
        List<Document> doc = documentService.updateDocument(docAndFile.getDocument());

        System.out.println("status: "+docAndFile.getDocument().getDoc_category_new());
        fileService.deleteFile(docAndFile.getDocument().getDoc_id()); //delete old file before add new file
        JSONArray files = new JSONArray();
        for(int i=0; i< docAndFile.getFile().length; i++){
            List<File>    file = fileService.updateFile(docAndFile.getFile()[i]);
            files.put(file);

            try {
                uploadObject(docAndFile.getFile()[i].getDoc_file_name(), "uploads/"+docAndFile.getFile()[i].getDoc_file_name());
            } catch (IOException e) {
                System.out.println("Error upload to google cloud: ");e.printStackTrace();
            }
        }
        System.out.println("files: "+files);
        Map<String, Object> map = new HashMap<>();
        map.put("doc",doc);
        map.put("files",files);

        try {
            deleteFile();
        } catch (IOException e) {
            System.out.println("Error to delete file: "); e.printStackTrace();
        }

        return map;
    }

    @GetMapping("/delete/{docId}")
    public String deleteDoc(@PathVariable("docId") String docId){
        System.out.println("docID: "+docId);
        String status = "";
        try {
            documentService.deleteDocument(docId);
            fileService.deleteFile(docId);
            status = "SUCCESS";
        }catch(Exception e){
            status = "ERROR";
        }
        return status;
    }

    @PostMapping("/files/delete")
    public void deleteFile() throws IOException {  //Delete all file in folder
//        Path fileToDeletePath = Paths.get("uploads/");
//        Files.delete(fileToDeletePath);
        Path folder = Paths.get("uploads");
        CleanFolder.clean(folder);
    }

    @GetMapping("/filter/{filterName}")
    public Map<String, Object> filter(@PathVariable("filterName") String filterName){
        List<Document> docs = documentService.getFilter(filterName);
        List<File> files = fileService.getFileFilter(filterName);
        Map<String, Object> map = new HashMap<>();
        map.put("data",docs);
        map.put("file",files);
        return map;
    }

    @GetMapping("/search/{searchVal}")
    public Map<String, Object> search(@PathVariable("searchVal") String searchVal){
        List<Document> docs = documentService.getSearch(searchVal);
        List<File> files = fileService.getFileSearch(searchVal);
        Map<String, Object> map = new HashMap<>();
        map.put("data",docs);
        map.put("file",files);
        return map;
    }


    //Upload to google cloud storage  //comment cause change storage of google cloud storage
    public static void uploadObject(String objectName, String filePath) throws IOException {
       /* // The ID of your GCP project
        String projectId = "copper-tempo-282707";

        // The ID of your GCS bucket
        String bucketName = "mydocfile";

        Storage storage = StorageOptions.newBuilder().setProjectId(projectId).build().getService();
        BlobId blobId = BlobId.of(bucketName, objectName);
        BlobInfo blobInfo = BlobInfo.newBuilder(blobId).build();
        storage.create(blobInfo, Files.readAllBytes(Paths.get(filePath)));

        System.out.println("File " + filePath + " uploaded to bucket " + bucketName + " as " + objectName);*/
    }

}
