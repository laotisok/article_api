package com.example.demo.controller;

import java.nio.file.Path;
import java.util.List;
import java.util.stream.Collectors;

import com.example.demo.exceptions.CleanFolder;
import com.google.api.gax.paging.Page;
import com.google.cloud.storage.*;
import org.apache.tomcat.util.http.fileupload.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.method.annotation.MvcUriComponentsBuilder;

import com.example.demo.model.FileInfo;
import com.example.demo.message.ResponseMessage;
import com.example.demo.service.FilesStorageService;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

@Controller
@RequestMapping(value = "/api/documents/file/") //@CrossOrigin("/api/documents/file/")
public class FilesController {

    @Autowired
    FilesStorageService storageService;

    @PostMapping("/upload")
    public ResponseEntity<ResponseMessage> uploadFile(@RequestParam("file") MultipartFile file) {
        String message = "";
        try {
            storageService.save(file);
            message = "Uploaded the file successfully: " + file.getOriginalFilename();
            return ResponseEntity.status(HttpStatus.OK).body(new ResponseMessage(message));
        } catch (Exception e) {
            message = "Could not upload the file: " + file.getOriginalFilename() + "!";
            return ResponseEntity.status(HttpStatus.EXPECTATION_FAILED).body(new ResponseMessage(message));
        }
    }

    @GetMapping("/files")
    public ResponseEntity<List<FileInfo>> getListFiles() {
        List<FileInfo> fileInfos = storageService.loadAll().map(path -> {
            String filename = path.getFileName().toString();
            String url = MvcUriComponentsBuilder
                    .fromMethodName(FilesController.class, "getFile", path.getFileName().toString()).build().toString();
            System.out.println("FILE: "+path.getFileName().toString());
//            try {
//                uploadObject(path.getFileName().toString(), "uploads/"+path.getFileName().toString());
//                //deleteFile();
//            } catch (IOException e) {
//                System.out.println("Error upload to google cloud: ");e.printStackTrace();
//            }
            return new FileInfo(filename, url);
        }).collect(Collectors.toList());

        return ResponseEntity.status(HttpStatus.OK).body(fileInfos);
    }

    @GetMapping("/files/{filename:.+}")
    @ResponseBody
    public ResponseEntity<Resource> getFile(@PathVariable String filename) {
        Resource file = storageService.load(filename);
        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + file.getFilename() + "\"").body(file);
    }

    @PostMapping("/files/delete")
    public void deleteFile() throws IOException {  //Delete all file in folder
//        Path fileToDeletePath = Paths.get("uploads/");
//        Files.delete(fileToDeletePath);
        Path folder = Paths.get("uploads");
        CleanFolder.clean(folder);
    }

}