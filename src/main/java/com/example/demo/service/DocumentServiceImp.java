package com.example.demo.service;

import com.example.demo.model.Category;
import com.example.demo.model.Document;
import com.example.demo.repository.DocumentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DocumentServiceImp implements DocumentService{
    private DocumentRepository documentRepository;

    @Autowired
    public void setDocumentRepository(DocumentRepository documentRepository) {
        this.documentRepository = documentRepository;
    }

    @Override
    public List<Document> findAll() {
        List<Document> list = this.documentRepository.findAll();
        return list;
    }

    @Override
    public List<Document> findAllMine(Long userId) {
        List<Document> list = this.documentRepository.findAllMine(userId);
        return list;
    }

    @Override
    public List<Document> findOne(String id) {
        List<Document> list = this.documentRepository.findOne(id);
        return list;
    }

    @Override
    public List<Document> addDocument(Document document) {
        List<Document> list = this.documentRepository.addDocument(document);
        return list;
    }

    @Override
    public List<Document> updateDocument(Document document) {
        List<Document> list = this.documentRepository.updateDocument(document);
        return list;
    }

    @Override
    public List<Document> addCategory(Document document) {
        List<Document> list = this.documentRepository.addCategory(document);
        return list;
    }

    @Override
    public List<Category> findAllMyCategory(Long userId) {
        List<Category> list = this.documentRepository.findAllMyCategory(userId);
        return list;
    }

    @Override
    public String deleteDocument(String docId) {
        return this.documentRepository.deleteDocument(docId);
    }

    @Override
    public List<Document> getFilter(String filterName) {
        return this.documentRepository.getFilter(filterName);
    }

    @Override
    public List<Document> getSearch(String searchVal) {
        return this.documentRepository.getSearch(searchVal);
    }

}
