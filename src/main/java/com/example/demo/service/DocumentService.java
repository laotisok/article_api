package com.example.demo.service;

import com.example.demo.model.Category;
import com.example.demo.model.Document;

import java.util.List;

public interface DocumentService {
    List<Document> findAll();
    List<Document> findOne(String id);
    List<Document> addDocument(Document document);
    List<Document> updateDocument(Document document);
    List<Document> findAllMine(Long userId);
    List<Document> addCategory(Document document);
    List<Category> findAllMyCategory(Long userId);
    List<Document> getFilter(String filterName);
    List<Document> getSearch(String searchVal);
    String deleteDocument(String id);
}
