package com.example.demo.service;

import com.example.demo.model.File;

import java.util.List;

public interface FileService {
    List<File> findAll();
    List<File> findFileOne(String id);
    List<File> addFile(File file);
    List<File> updateFile(File file);
    List<File> getFileFilter(String filterName);
    List<File> getFileSearch(String searchVal);
    String deleteFile(String docId);
}
