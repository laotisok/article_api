package com.example.demo.service;

import com.example.demo.model.File;
import com.example.demo.repository.FileRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class FileServiceImp implements FileService{
    FileRepository fileRepository;

    @Autowired
    public void setFileRepository(FileRepository fileRepository){
        this.fileRepository = fileRepository;
    }

    @Override
    public List<File> findAll() {
        List<File> files =  this.fileRepository.findAll();
        return files;
    }

    @Override
    public List<File> findFileOne(String id) {
        List<File> files =  this.fileRepository.findFileOne(id);
        return files;
    }

    @Override
    public List<File> addFile(File file) {
        List<File> files = this.fileRepository.addFile(file);
        return files;
    }

    @Override
    public List<File> updateFile(File file) {
        List<File> files = this.fileRepository.updateFile(file);
        return files;
    }

    @Override
    public List<File> getFileFilter(String filterName) {
        return this.fileRepository.getFileFilter(filterName);
    }

    @Override
    public List<File> getFileSearch(String searchVal) {
        return this.fileRepository.getFileSearch(searchVal);
    }

    @Override
    public String deleteFile(String docId) {
        return this.fileRepository.deleteFile(docId);
    }
}
