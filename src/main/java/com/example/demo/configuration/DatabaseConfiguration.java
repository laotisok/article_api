package com.example.demo.configuration;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import javax.sql.DataSource;

@Configuration
@MapperScan("com.example.demo.repository")
public class DatabaseConfiguration {

    @Bean
    public DataSource dataSource(){
        /*DriverManagerDataSource driverManagerDataSource = new DriverManagerDataSource();
        driverManagerDataSource.setDriverClassName("org.postgresql.Driver");
        driverManagerDataSource.setUrl("jdbc:postgresql://35.240.171.96/mydoc");
        driverManagerDataSource.setUsername("laoti");
        driverManagerDataSource.setPassword("laotisok@8899");
        return driverManagerDataSource;*/

        DriverManagerDataSource driverManagerDataSource = new DriverManagerDataSource();
        driverManagerDataSource.setDriverClassName("org.postgresql.Driver");
        driverManagerDataSource.setUrl("jdbc:postgresql://localhost:5433/mydoc");
        driverManagerDataSource.setUsername("postgres");
        driverManagerDataSource.setPassword("awesome8899");
        return driverManagerDataSource;
    }
}
