package com.example.demo.repository;

import com.example.demo.model.File;
import com.example.demo.repository.provider.DocumentProvider;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.SelectProvider;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface FileRepository {

    @Select("select * from doc_files where doc_file_status = 1")
    List<File> findAll();

    @SelectProvider(method="findFileOne", type= DocumentProvider.class)
    List<File> findFileOne(String id);

    @SelectProvider(method="addFile", type = DocumentProvider.class)
    List<File> addFile(File file);

    @SelectProvider(method="updateFile", type = DocumentProvider.class)
    List<File> updateFile(File file);

    @SelectProvider(method="getFileFilter", type = DocumentProvider.class)
    List<File> getFileFilter(String filterName);

    @SelectProvider(method="getFileSearch", type = DocumentProvider.class)
    List<File> getFileSearch(String searchVal);

    @SelectProvider(method="deleteFile", type = DocumentProvider.class)
    String deleteFile(String docId);
}
