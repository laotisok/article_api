package com.example.demo.repository;

import com.example.demo.model.Category;
import com.example.demo.model.Document;
import com.example.demo.repository.provider.DocumentProvider;
import org.apache.ibatis.annotations.*;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DocumentRepository {
    //, dt.doc_type_name inner join doc_type as dt on di.doc_type_id = dt.id
    @Select("select di.*, du.username, dc.doc_category_name from doc_items as di \n" +
            "inner join doc_users as du on di.user_id = du.id \n" +
            "left join doc_category as dc on di.doc_category_id = dc.doc_category_id \n" +
            "where di.doc_status = 1")
    List<Document> findAll();

    @SelectProvider(method="findAllMine", type = DocumentProvider.class)
    List<Document> findAllMine(Long userId);

    @SelectProvider(method="findOne", type = DocumentProvider.class)
    List<Document> findOne(String id);

    @SelectProvider(method="addDocument", type = DocumentProvider.class)
    List<Document> addDocument(Document document);

    @SelectProvider(method="updateDocument", type = DocumentProvider.class)
    List<Document> updateDocument(Document document);

    @SelectProvider(method = "addCategory", type = DocumentProvider.class)
    List<Document> addCategory(Document document);

    @SelectProvider(method="findAllMyCategory", type = DocumentProvider.class)
    List<Category> findAllMyCategory(Long userId);

    @SelectProvider(method="deleteDocument", type = DocumentProvider.class)
    String deleteDocument(String docId);

    @SelectProvider(method="getFilter", type = DocumentProvider.class)
    List<Document> getFilter(String filterName);

    @SelectProvider(method="getSearch", type = DocumentProvider.class)
    List<Document> getSearch(String searchVal);
}
