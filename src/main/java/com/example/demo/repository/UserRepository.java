package com.example.demo.repository;

import com.example.demo.model.User;
import com.example.demo.repository.provider.UserProvider;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.SelectProvider;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import java.util.List;

@Repository
public interface UserRepository {

//    @Select("select * from users where username = ? and password = ? and user_status = 1")
    @SelectProvider(method="login", type = UserProvider.class)
    List<User> login(String username, String password);
}
