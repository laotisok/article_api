package com.example.demo.repository.provider;

import com.example.demo.model.Document;
import com.example.demo.model.File;

public class DocumentProvider {

    public String findOne(String id){
        return "select di.*, du.username, dc.doc_category_name from doc_items as di " +
                "inner join doc_users as du on di.user_id = du.id  " +
                "inner join doc_category as dc on di.doc_category_id = dc.doc_category_id where di.doc_id='"+id+"'";
    }

    public String findAllMine(Long userId){
        return "select di.id, di.user_id, di.doc_name, di.doc_content, di.doc_created_date, di.doc_type_name, di.doc_view_amount, di.doc_id , du.username, dc.doc_category_id, dc.doc_category_name from doc_items as di \n" +
                "inner join doc_users as du on di.user_id = du.id left join doc_category as dc on di.doc_category_id=dc.doc_category_id where di.doc_status = 1 and di.user_id='"+userId+"'";
    }

    public String findAllMyCategory(Long userId){
        return "select * from doc_category where doc_category_status = 1 and doc_user_id='"+userId+"'";
    }

    public String findFileOne(String id){
        return "select * from doc_files where doc_id='"+id+"' and doc_file_status=1";
    }

    public String addDocument(Document document){
        String docCategoryId    = document.getDoc_category_id();
        Long   userId           = document.getUser_id();
        String docName          = document.getDoc_name();
        String docId            = document.getDoc_id();
        String docContent       = document.getDoc_content();
        String docCreatedDate   = document.getDoc_created_date();
        String docStatus        = "1";
        String docTypeName      = document.getDoc_type_name();
        Long docViewAmount      = 0L;
        return "insert into doc_items(doc_category_id, user_id, doc_id, doc_name, doc_content, doc_created_date, doc_status, doc_type_name, doc_view_amount)" +
                "values('"+docCategoryId+"','"+userId+"','"+docId+"','"+docName+"','"+docContent+"','"+docCreatedDate+"','"+docStatus+"','"+docTypeName+"','"+docViewAmount+"')";
    }

    public String addFile(File file){
        String docId = file.getDoc_id();
        String docFileName = file.getDoc_file_name();
        String docFilePath = file.getDoc_file_path();
        int docFileStatus = 1;
        String docCreatedDate = file.getDoc_file_created_date();
        return "insert into doc_files(doc_id, doc_file_name, doc_file_path, doc_file_status, doc_file_created_date)"+
                "values('"+docId+"','"+docFileName+"','"+docFilePath+"','"+docFileStatus+"','"+docCreatedDate+"')";
    }

    public String addCategory(Document document){
        System.out.println("docCategoryId: "+document.getDoc_category_id());
        System.out.println("docCategoryName: "+document.getDoc_category_name());
        String docCategoryId    = document.getDoc_category_id();
        String docCategoryName  = document.getDoc_category_name();
        Long   userId           = document.getUser_id();
        int docCategoryStatus   = 1;
        return "insert into doc_category(doc_category_id, doc_category_name, doc_user_id, doc_category_status)" +
                "values('"+docCategoryId+"','"+docCategoryName+"','"+userId+"','"+docCategoryStatus+"')";
    }

    public String deleteDocument(String docId){
        return "UPDATE doc_items set doc_status=0 where doc_id='"+docId+"'";
    }

    public String deleteFile(String docId){
        return "UPDATE doc_files set doc_file_status=0 where doc_id='"+docId+"'";
    }

    public String updateDocument(Document document){
        String docCategoryId = document.getDoc_category_id();
        Long docUserId = document.getUser_id();
        String docId = document.getDoc_id();
        String docName = document.getDoc_name();
        String docContent = document.getDoc_content();
        String docCreatedDate = document.getDoc_created_date();
        String docTypeName = document.getDoc_type_name();
        String sql = "UPDATE doc_items set doc_category_id='"+docCategoryId+"', user_id='"+docUserId+"', " +
                "doc_name='"+docName+"', doc_content='"+docContent+"', doc_created_date='"+docCreatedDate+"', " +
                "doc_type_name='"+docTypeName+"' where doc_id='"+docId+"'";

        System.out.println("sql: "+sql);
        return sql;
    }

    public String updateFile(File file){
        String docId = file.getDoc_id();
        String docFileName = file.getDoc_file_name();
        String docFilePath = file.getDoc_file_path();
        String docCreatedDate = file.getDoc_file_created_date();
        return "insert into doc_files(doc_id, doc_file_name, doc_file_path, doc_file_status, doc_file_created_date)"+
                "values('"+docId+"','"+docFileName+"','"+docFilePath+"','1','"+docCreatedDate+"')";
    }

    public String getFilter(String filterName){
        String sql= "select di.*, du.username, dc.doc_category_name from doc_items as di " +
                    "inner join doc_users as du on di.user_id = du.id " +
                    "inner join doc_category as dc on di.doc_category_id = dc.doc_category_id " +
                    "where di.doc_status = 1 " +
                    "order by " +
                    "case when 'popular' = '"+filterName+"'  then di.doc_view_amount end desc, " +
                    "case when 'new'= '"+filterName+"'  then di.doc_created_date end desc";
        return sql;
    }

    public String getFileFilter(String filterName){
        String sql= "select * from doc_files as df " +
                    "inner join doc_items as di on di.doc_id = df.doc_id " +
                    "where df.doc_file_status = 1 " +
                    "order by " +
                    "case when 'popular' = '"+filterName+"'  then di.doc_view_amount end desc, " +
                    "case when 'new'= '"+filterName+"'  then di.doc_created_date end desc";
        return sql;
    }

    public String getSearch(String searchVal){
        String sql= "select di.*, du.username, dc.doc_category_name from doc_items as di " +
                "inner join doc_users as du on di.user_id = du.id " +
                "inner join doc_category as dc on di.doc_category_id = dc.doc_category_id " +
                "where di.doc_status = 1 and (LOWER (di.doc_name) like LOWER ('%"+searchVal+"%') or LOWER (di.doc_type_name) like LOWER ('%"+searchVal+"%'))";
        return sql;
    }

    public String getFileSearch(String searchVal){
        String sql= "select * from doc_files as df " +
                "inner join doc_items as di on di.doc_id = df.doc_id " +
                "where df.doc_file_status = 1 and di.doc_status =1 and (LOWER (di.doc_name) like LOWER ('%"+searchVal+"%') or LOWER (di.doc_type_name) like LOWER ('%"+searchVal+"%'))";
        return sql;
    }

}
